@include('includes/header')
@include('includes/menu')

<div class="container">
    <div class="row">
        <div class="col-md-4 offset-md-4">
            <input type="text" id="new_url" class="form-control form-control-sm" placeholder="Informe a URL">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 offset-md-4 d-grid my-3">
            <button type="button" class="btn btn-sm btn-success" onclick="adicionarUrl()">Adicionar URL</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table id="table_urls" class="table table-sm table-bordered table-hover table-striped table_center">
                <thead>
                    <tr>
                        <th>URL</th>
                        <th>HTTP</th>
                        <th>Code</th>
                        <th width="10%"></th>
                        <th width="10%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="5">Não conseguimos localizar nenhuma URL.</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('js/home.js')}}"></script>
@endpush

@include('includes/footer')
