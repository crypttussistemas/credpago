</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('plugins/funcoes.js')  }}" type="text/javascript"></script>
<script>
    const access_token = '{{Session::get('TOKEN')}}';
    const base_url = '{{\Illuminate\Support\Facades\URL::to('/')}}';
    var _token = $("input[name='_token']").val();
</script>
@stack('scripts')
</html>
