<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <title>CredPago</title>
</head>
<style>
    .link_possui_cadastro {
        text-align: center;
        text-decoration: none;
        font-weight: 600;
    }
</style>
<body>
@csrf
<div class="row py-5">
    <div class="col-md-4 offset-md-4">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="nome" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-12">
                        <label for="email">E-Mail</label>
                        <input type="email" name="email" id="email" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-12">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" id="senha" class="form-control form-control-sm">
                    </div>
                    <div class="col-md-12 d-grid py-3">
                        <button type="button" class="btn btn-sm btn-primary" onclick="logar()">Criar Conta</button>
                    </div>
                    <div class="col-md-12 d-grid py-3">
                        <a href="{{route('login')}}" class="link_possui_cadastro">Já possui cadastro</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('plugins/funcoes.js')  }}" type="text/javascript"></script>
<script src="{{ asset('js/criar_conta.js')  }}" type="text/javascript"></script>
<script>
    const access_token = '';
    const base_url = '{{\Illuminate\Support\Facades\URL::to('/')}}';
</script>
</html>
