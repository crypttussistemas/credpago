<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace' => 'Api'
], function () {
    Route::post('login', 'AuthController@login'); // Faz login e retorna o JWT TOKEN
    Route::post('logout', 'AuthController@logout'); // Faz logout e destroi o JWT TOKEN
    Route::post('refresh', 'AuthController@refresh'); // Devolve um novo JWT TOKEN
    Route::post('me', 'AuthController@me'); // Devolve os dados do usuário ligado aquele JWT TOKEN

    Route::post('url', 'UrlsController@store'); // Cadastra a nova URL
    Route::put('url/{id}', 'UrlsController@update'); // Atualiza a URL
    Route::delete('url/{id}', 'UrlsController@destroy'); // Exclui a URL
    Route::get('url', 'UrlsController@index'); // Retorna todas as URL's cadastradas
    Route::get('url/{id}', 'UrlsController@show'); // Retorna uma URL especifica de acordo com o ID passado
});

Route::post('criar_conta', [UsersController::class, 'store'])->middleware('guest'); // Insere um novo usuario
