<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::get('home', 'HomeController@index')->name('home');
Route::post('set_session', 'LoginController@set_session')->name('set_session');
Route::get('criar_conta', function() {
    return view('criar_conta');
})->name('criar_conta');

Route::get('logout', 'LoginController@logout');

Route::get('get/urls', function(){
    $token = \Illuminate\Support\Facades\Session::get('TOKEN');
   \App\Jobs\CheckUrls::dispatch($token)->delay(now()->addSeconds(60));
});
