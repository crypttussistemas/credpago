const logar = () => {
    let email = $("#email").val();
    let senha = $("#senha").val();
    let _token = $("input[name='_token']").val();

    let obrigatorio = [email, senha];

    if(in_array('', obrigatorio)) {
        alert('E-Mail e Senha devem ser informados.');
        return false;
    }

    let data = requisicao('/api/auth/login', {email, password: senha, _token});

    if(data.access_token) {
        if(requisicao('/set_session', {access_token: data.access_token, _token}))
            window.location.href = 'home';
    }
}
