const logar = () => {
    let nome = $("#nome").val();
    let email = $("#email").val();
    let senha = $("#senha").val();
    let _token = $("input[name='_token']").val();

    let obrigatorio = [nome, email, senha];

    if(in_array('', obrigatorio)) {
        alert('Todos os campos devem ser informados.');
        return false;
    }

    let data = requisicao('/api/criar_conta', {nome, email, senha, _token});

    if(data.response) {
        window.location.href = '/';
    }
}
