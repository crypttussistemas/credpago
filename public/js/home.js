$(document).ready(function() {
   buscarUrls();

   setInterval(function () {buscarUrls();}, 60000);
});

const editarUrl = (id) => {
    $("#table_urls tbody tr #td_"+id).html('<input type="text" class="form-control form-control-sm" id="atualizarUrlInput">');
    $("#table_urls tbody tr #td_button_"+id).html("<button type='button' class='btn btn-sm btn-success' onclick='atualizarUrl("+id+")'>Salvar URL</button>");
}

const adicionarUrl = () => {
    let url = $("#new_url").val();

    if(empty(url)) {
        alert('Você deve informar a URL que deseja adicionar.');
        $("#new_url").focus();
        return false;
    }

    let result = requisicao('/api/auth/url', {url});

    if(result.response) {
        alert(result.message);
        buscarUrls();
    } else {
        alert(result.message)
    }
}

const buscarUrls = () => {
    let result = requisicao('/api/auth/url', {_token}, 'get');

    let html = "";

    if(result.response) {
        let data = result.data;

        data.map(function(dados) {
            html += "<tr>";
            html += "<td id='td_"+dados.id+"'>"+dados.url+"</td>";
            html += "<td>"+dados.response_http+"</td>";
            html += "<td>"+dados.status_code+"</td>";
            html += "<td id='td_button_"+dados.id+"'><button type='button' class='btn btn-sm btn-primary' onclick='editarUrl("+dados.id+")'>Editar URL</button></td>";
            html += "<td id='td_button_"+dados.id+"'><button type='button' class='btn btn-sm btn-danger' onclick='deletarUrl("+dados.id+")'>Deletar URL</button></td>";
            html += "</tr>";
        });

        $("#table_urls tbody tr").remove();
        $("#table_urls tbody").append(html);
    } else {
        html += "<tr>";
        html += "<td colspan='5'>"+result.message+"</td>";
        html += "</tr>";

        $("#table_urls tbody tr").remove();
        $("#table_urls tbody").append(html);
    }
}


const atualizarUrl = (id) => {
    let url = $("#atualizarUrlInput").val();

    let result = requisicao('/api/auth/url/'+id, {url: url, _token}, 'PUT');

    if(result.response) {
        alert(result.message);
        buscarUrls();
    } else {
        alert(result.message);
    }
}

const deletarUrl = (id) => {
    if(confirm('Deseja excluir permanentemente esta URL?')) {
        let result = requisicao('/api/auth/url/'+id, {_token}, 'DELETE');

        if(result.response) {
            alert(result.message);
            buscarUrls();
        } else {
            alert(result.message);
        }
    }
}

const logout = () => {
    requisicao('/api/auth/logout');
    window.location.href = 'logout';
}
