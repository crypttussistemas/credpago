function in_array(value, array)
{
    let index = 0;

    for( index = 0; index <= array.length; index++ )
    {
        if(array[index] == value)
            return true;
    }

    return false;
}

function empty(value)
{
    if(value == "" || value == null || value == undefined || value == " " || value == false)
        return true;

    return false;
}

/**
 * Realiza requisições AJAX personalizadas.
 *
 * @param {string} url - ROTA onde a requisição deve apontar
 * @param {Array} [dados] - Parametros que serão enviados na requisição
 * @param {string} [type] - Define o tipo da requisição se é POST ou GET
 */
function requisicao(url, dados = "", type = 'post')
{
    let lblButton = $(".button_request").html();

    var data = $.ajax({
        cache: true,
        url: base_url+url,
        async: false,
        type: type,
        datatype: 'json',
        data: dados,
        statusCode: {
            500: function(){
                $(".button_request").removeAttr('disabled');
            }
        },
        beforeSend: function(xhr){
            xhr.setRequestHeader('Authorization', 'Bearer '+access_token);
            $(".button_request").attr('disabled', 'disabled').html('Carregando...');
        },
        success: function()
        {
            $(".button_request").removeAttr('disabled').html(lblButton);
        }
    });

    return JSON.parse(data.responseText);
}
