<?php

namespace App\Console\Commands;

use App\Models\Urls;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Session;

class CheckUrlCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:url';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checa as URLs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = Urls::get();

        foreach($result as $row) {
            // Inicia o CURL
            $ch = curl_init();

            curl_setopt ( $ch, CURLOPT_URL, $row->url );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
            curl_exec($ch);
            $response['url'] = $row->url;
            $response['response_http'] = curl_getinfo($ch, CURLINFO_SCHEME);
            $response['status_code'] = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

            // Fecha o CURL
            curl_close( $ch );

            Urls::where('id', $row->id)->update($response);
        }
        return 'Checando URLs com exito.';
    }
}
