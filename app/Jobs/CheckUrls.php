<?php

namespace App\Jobs;

use App\Models\Urls;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class CheckUrls implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Inicia
        $curl = curl_init();

        // Configura
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://localhost:8000/api/auth/url',
            CURLOPT_HTTPHEADER => ['Authorization: Bearer '.$this->token]
        ]);
        // Envio e armazenamento da resposta
        $response = curl_exec($curl);
        $result = json_decode($response);
        $result = $result->data;

        // Fecha e limpa recursos
        curl_close($curl);

        foreach($result as $row) {
            // Inicia
            $curl = curl_init();

            // Configura
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://localhost:8000/api/auth/url/'.$row->id,
                CURLOPT_HTTPHEADER => ['Authorization: Bearer '.$this->token]
            ]);
            // Envio e armazenamento da resposta
            curl_exec($curl);
            // Fecha e limpa recursos
            curl_close($curl);
        }
    }
}
