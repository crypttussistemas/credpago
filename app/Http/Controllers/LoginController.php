<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function set_session(Request $request)
    {
        Session::put('TOKEN', $request->access_token);

        if(Session::has('TOKEN'))
            return response()->json(['response' => true]);
        else
            return response()->json(['response' => false]);
    }

    public function logout()
    {
        Session::flush();
        return redirect()->route('login');
    }
}
