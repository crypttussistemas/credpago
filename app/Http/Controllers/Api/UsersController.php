<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    protected $users;

    function __construct(User $users) {
        $this->users = $users;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $validation = $request->validate(['nome' => 'required', 'email' => 'required|unique:users', 'senha' => 'required']);

            if(!$validation)
                return response()->json(['response' => false, 'message' => 'todos os campos são de preenchimento obrigatório']);

            $dados['name'] = trim($request->nome);
            $dados['email'] = trim($request->email);
            $dados['password'] = Hash::make(trim($request->senha));
            $dados['email_verified_at'] = now();
            $dados['remember_token'] = Str::random(10);

            $this->users->create($dados);
            return response()->json(['response' => true, 'message' => 'Usuário cadastrado com sucesso.', 'error' => NULL]);
        } catch(\Exception $e) {
            return response()->json(['response' => false, 'message' => 'Não foi possivel cadastrar esse usuario.', 'error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
