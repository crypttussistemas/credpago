<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Urls;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function response;

class UrlsController extends Controller
{
    protected $urls;

    function __construct(Urls $urls) {
        $this->middleware('auth:api');
        $this->urls = $urls;
    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->urls->get();

            return response()->json(['response' => true, 'message' => 'Dados localizados com sucesso', 'error' => NULL, 'data' => $data]);
        } catch( Exception $e ) {
            return response()->json(['response' => false, 'message' => 'Não conseguimos concluir sua solicitação.', 'error' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        try {

            // Valida se o valor passado da URL segue o padrão de uma URL válida
            if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$request->url))
                return response()->json(['response' => false, 'message' => 'A URL passada não contém o formato correto.']);

            $dados_url = $this->url_test(trim($request->url));

            $this->urls->create($dados_url);

            return response()->json(['response' => true, 'message' => 'URL cadastrada com sucesso.', 'error' => NULL]);
        } catch( Exception $e ) {
            return response()->json(['response' => false, 'message' => 'Não foi possível cadastrar nova URL.', 'error' => $e->getMessage()]);
        }
    }

    /**
     * Faz o acesso da URL para verificar o status da mesma
     *
     * @param $url
     * @return array
     */
    private function url_test( $url ): array
    {
        $timeout = 10;

        // Inicia o CURL
        $ch = curl_init();

        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
        curl_exec($ch);
        $response['url'] = $url;
        $response['response_http'] = curl_getinfo($ch, CURLINFO_SCHEME);
        $response['status_code'] = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

        // Fecha o CURL
        curl_close( $ch );

        return $response; // Retorna o resultado obtido no CURL
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        try {
            $data = $this->urls->find($id)->first();
            return response()->json(['response' => true, 'message' => 'Dados localizados com sucesso', 'error' => NULL, 'data' => $data]);
        } catch( Exception $e ) {
            return response()->json(['response' => false, 'message' => 'Não conseguimos concluir sua solicitação.', 'error' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        try {
            // Valida se o valor passado da URL segue o padrão de uma URL válida
            if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$request->url))
                return response()->json(['response' => false, 'message' => 'A URL passada não contém o formato correto.']);

            $dados_url = $this->url_test(trim($request->url));

            $this->urls->find($id)->update($dados_url);
            return response()->json(['response' => true, 'message' => 'URL atualizada com sucesso.', 'erro' => NULL]);
        } catch( Exception $e ) {
            return response()->json(['response' => false, 'message' => 'Não foi possivel atualizar a URL.', 'error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $this->urls->find($id)->delete();

            return response()->json(['response' => true, 'message' => 'URL excluída com sucesso.', 'erro' => NULL]);
        } catch( Exception $e ) {
            return response()->json(['response' => false, 'message' => 'Não foi possivel excluir URL.', 'error' => $e->getMessage()]);
        }
    }
}
