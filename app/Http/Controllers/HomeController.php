<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    function __construct() {
        if(!Session::has('TOKEN'))
            return redirect()->route('login');
    }

    public function index()
    {
        return view('home');
    }
}
